from flask import Flask, render_template, request, redirect, url_for
import enssatrooms
import datetime
import json
import arrow

app = Flask(__name__)


# Need a way to get the timeline upto date
enssatrooms.load_calendar()
tl = enssatrooms.c.timeline  # todo: update the timeline every day


@app.route("/")
def hello():
    # Unique entry point for the web app
    return render_template("index.html")


# API for javascript calls
@app.route("/api/rooms/unoccuped")
def api_rooms():
    # get the instant from the request in GET method in the format YYYY-MM-DD:MM:SS
    instant = request.args.get("instant")
    if instant:
        # string to datetime to arrow
        dt = datetime.datetime.strptime(instant, "%Y-%m-%d:%H:%M")
        # split dt to Y, m, d, H, M and pass it to arrow.get

        instant = arrow.get(dt.year, dt.month, dt.day, dt.hour, dt.minute)
    else:
        instant = arrow.now()

    rooms = enssatrooms.get_unoccuped_rooms(timeline=tl, instant=instant)

    # Return a json object
    return json.dumps(rooms)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8000)
