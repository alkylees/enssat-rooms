from ics import Calendar, Event
import requests
import datetime
import time
import re
import arrow

# for dev only (to save the calendar bc it takes time to fetch it)
import pickle
import os

# list of rooms in ENSSAT
ROOMS = []

# check if the calendar is already saves


global c  # Todo: remove this global variable bc it's bad
c = None


def load_calendar():

    global c

    if all([
        os.path.isfile("calendar.pickle"),
        datetime.date.fromtimestamp(os.path.getmtime(
            "calendar.pickle")) > datetime.date.today()
    ]):

        print("Loading calendar from pickle...")
        with open("calendar.pickle", "rb") as f:
            c = pickle.load(f)
        print("Calendar loaded")
    else:
        # fetch the calendar
        ICAL_URL = "https://planning.univ-rennes1.fr/jsp/custom/modules/plannings/KYNXdqWv.shu"

        print("Fetching calendar...")
        txt = requests.get(ICAL_URL).text
        print("Calendar fetched : {txt}".format(txt=txt[:100]))
        print("Parsing calendar...")

        # TODO: Do not parse the whole calendar... It takes too much time

        t1 = time.time()
        c = Calendar(txt)
        t2 = time.time()
        print("Calendar parsed in {t} seconds".format(t=t2-t1))

        # save the calendar
        print("Saving calendar to pickle...")
        with open("calendar.pickle", "wb") as f:
            pickle.dump(c, f)
        print("Calendar saved")

        events = list(c.timeline)


# function to get the room from the event description
def parse_room(event):
    # regex to get the room from the description
    room_regex = re.compile(r"([0-9]{3}[A-Z]{1})")
    room = room_regex.search(event.location)
    if room:
        return room.group(1)
    else:
        return None


def get_all_rooms():
    """Get all the rooms in the calendar"""
    ROOMS = []
    for event in events:
        room = parse_room(event)
        if room not in ROOMS:
            ROOMS.append(room)

    return ROOMS


def get_occuped_rooms(timeline, instant=arrow.now()):
    ROOMS = get_all_rooms()
    occuped_rooms = [parse_room(event) for event in c.timeline if event.begin.date(
    ) == instant.date() and event.end > instant and event.begin <= instant]
    return occuped_rooms


def get_unoccuped_rooms(timeline, instant=arrow.now()):
    ROOMS = get_all_rooms()
    occuped_rooms = get_occuped_rooms(timeline)
    unoccuped_rooms = [room for room in ROOMS if room not in occuped_rooms]
    return unoccuped_rooms


if __name__ == "__main__":
    instant = arrow.get(2023, 9, 14, 10, 15, 0)
    print(
        f"Rooms occuped at {instant} : {get_occuped_rooms(c.timeline, instant)}")
